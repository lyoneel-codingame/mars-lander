### This is one of my codingames solutions

You may find this useful, I leave in this repository to track a history of my in Codingame.com.

Few things that I want to say to anyone that found this repository:

* The rules are not going to be written here, because I don't want to replace the  original content page with this repository.
* Is just my own history or possible solution(s) to the problem, probably is not the best, or only solution.
* This is a generic readme to all my codingame repositories.
* May I leave a second program that can be run in your computer, normally called "local".

All solutions are MIT licensed, see license details in LICENSE.md